import './styles/main.less';

import { persistor, store } from 'store';

import Dashboard from 'containers/group/Dashboard';
import LoginScreen from 'containers/screens/Login';
import MainApp from 'containers/group/MainApp';
import NotFoundScreen from 'containers/screens/NotFound';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { Provider } from 'react-redux';
import React from 'react';

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <MainApp
          loginComponent={LoginScreen}
          notFoundComponent={NotFoundScreen}
          dashboardComponent={() => (
            <Dashboard notFoundComponent={NotFoundScreen} />
          )}
        />
      </PersistGate>
    </Provider>
  );
}

export default App;
