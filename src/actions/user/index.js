import { createActions } from 'redux-actions';
import ActionTypes from './type';

export const { userLogin: login, userLogout: logout } = createActions({
  [ActionTypes.USER_LOGIN]: param => ({ ...param }),
  [ActionTypes.USER_CREATE]: param => ({ ...param }),
});
