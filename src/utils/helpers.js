// @flow

/**
 * Check Permission Function
 * @param {Array<string>} userPermissions
 * @param {Array<string>} allowedPermissions
 * @returns {boolean}
 */
function checkPermissions(
  userPermissions: Array<string>,
  allowedPermissions: Array<string>,
  type: string,
): boolean {
  if (allowedPermissions && allowedPermissions.length > 0) {
    if (type === 'some') {
      return allowedPermissions.some(permission =>
        userPermissions.includes(permission),
      );
    }
    return allowedPermissions.every(permission =>
      userPermissions.includes(permission),
    );
  }
  return true;
}

export default { checkPermissions };
