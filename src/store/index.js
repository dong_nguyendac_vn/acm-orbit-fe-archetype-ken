import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';

import history from 'utils/history';
import rootReducer from 'reducers/index';
import rootSaga from 'sagas/index';
import { routerMiddleware } from 'connected-react-router';
import storage from 'redux-persist/lib/storage';
import middleware, { sagaMiddleware } from './middleware';

const reducer = persistReducer(
  {
    key: 'rrsb', // key is required
    storage, // storage is now required
    whitelist: ['app', 'user'],
  },
  combineReducers({
    ...rootReducer,
  }),
);

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/* istanbul ignore next */
const configStore = (initialState = {}) => {
  const store = createStore(
    reducer,
    initialState,
    composeEnhancer(applyMiddleware(routerMiddleware(history), ...middleware)),
  );

  sagaMiddleware.run(rootSaga);

  return {
    persistor: persistStore(store),
    store,
  };
};

const { store, persistor } = configStore();

global.store = store;

export { store, persistor };
