import Dashboard from 'components/group/Dashboard';
import { connect } from 'react-redux';
import { memo } from 'react';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps() {
  return {};
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const ConnectedDashBoard = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(Dashboard);

export default memo(ConnectedDashBoard);
