import MainApp from 'components/group/MainApp';
import { connect } from 'react-redux';
import { memo } from 'react';

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps() {
  return {};
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const ConnectedMainApp = connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(MainApp);

export default memo(ConnectedMainApp);
