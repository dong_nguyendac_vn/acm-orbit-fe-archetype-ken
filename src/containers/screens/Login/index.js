import LoginScreen from 'components/screens/Login';
import { ORBForm } from 'components/basic/ORB';
import { connect } from 'react-redux';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps() {
  return {};
}

function mergeProps(stateProps, dispatchProps, ownProps) {
  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
  };
}

const LoginScreenForm = ORBForm.create({ form: 'login-form' })(LoginScreen);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  mergeProps,
)(LoginScreenForm);
