import AccessControl from 'components/basic/AccessControl';
import { connect } from 'react-redux';
import { memo } from 'react';

function mapStateToProps(state) {
  return {
    userPermissions: state.user.auth && state.user.auth.permissions,
    user: state.user,
  };
}

function mapDispatchToProps() {
  return {};
}

const ConnectedAccessControl = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AccessControl);

export default memo(ConnectedAccessControl);
