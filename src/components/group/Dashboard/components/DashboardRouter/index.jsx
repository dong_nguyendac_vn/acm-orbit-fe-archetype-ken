import React, { Suspense } from 'react';
import { Redirect, Route } from 'react-router-dom';

import { AppConfig } from 'config';
import { ConnectedRouter } from 'connected-react-router';
import PropTypes from 'prop-types';
import { Switch } from 'react-router';
import checkPermissions from 'utils/helpers';
import history from 'utils/history';

export const LoadingMessage = () => "I'm loading...";

export function DashBoardRouter(props) {
  const { userPermissions, routes, NotFoundPage } = props;
  return (
    <ConnectedRouter history={history}>
      <Suspense fallback={<LoadingMessage />}>
        <Switch>
          {routes.map(route => {
            const { path, exact, permission, } = route;
            const { values, type } = permission || { values: [], type: 'some' };
            const permited = checkPermissions(
              userPermissions,
              values || [],
              type || 'some',
            );
            return permited ? (
              <Route
                exact={exact}
                key={path}
                path={`${AppConfig.appPrefix}${path}`}
              />
            ) : (
              <Redirect
                key={path}
                path={`${AppConfig.appPrefix}${path}`}
                to={{
                  pathname: `${AppConfig.appPrefix}/404`,
                }}
              />
            );
          })}
          <Route component={NotFoundPage} />
        </Switch>
      </Suspense>
    </ConnectedRouter>
  );
}

DashBoardRouter.propTypes = {
  NotFoundPage: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
      .isRequired,
  routes: PropTypes.arrayOf(PropTypes.object).isRequired,
  userPermissions: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default DashBoardRouter;
