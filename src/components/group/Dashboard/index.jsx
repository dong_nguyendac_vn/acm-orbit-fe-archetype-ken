import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import { AppRoutes as routes } from 'config';
import DashboardRouter from './components/DashboardRouter';

class DashBoard extends PureComponent {

  render() {
    const { notFoundComponent } = this.props;
    return (
      <div>
        <DashboardRouter
          userPermissions={[]}
          routes={routes}
          NotFoundPage={notFoundComponent}
        />
      </div>
    );
  }
}
DashBoard.propTypes = {
    notFoundComponent: PropTypes.shape.isRequired,
  };
  
export default DashBoard;
