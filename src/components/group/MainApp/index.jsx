import React, { PureComponent } from 'react';

import { AppHolder } from 'styles';
import { AppTheme } from 'config';
import { ConnectedRouter } from 'connected-react-router';
import PrivateRouter from 'components/basic/Router/PrivateRouter';
import PropTypes from 'prop-types';
import PublicRouter from 'components/basic/Router/PublicRouter';
import { Switch } from 'react-router';
import { ThemeProvider } from 'styled-components';
import history from 'utils/history';

class MainApp extends PureComponent {


  render() {
    const {
      loginComponent,
      dashboardComponent,
      notFoundComponent,
      user,
    } = this.props;
    const { isAuthenticated } = user;
  
    return (
      <ThemeProvider theme={AppTheme}>
        <AppHolder>
          <ConnectedRouter history={history}>
            <Switch>
              <PublicRouter
                exact
                path="/"
                isAuthenticated={isAuthenticated}
                component={loginComponent}
              />
              <PrivateRouter
                isAuthenticated={isAuthenticated}
                component={dashboardComponent}
              />
              <PublicRouter
                exact
                path="/template"
                isAuthenticated={false}
                component={loginComponent}
              />
              <PublicRouter
                isAuthenticated={isAuthenticated}
                component={notFoundComponent}
              />
            </Switch>
          </ConnectedRouter>
        </AppHolder>
      </ThemeProvider>
    );
  }
}
MainApp.defaultProps = {
}
 MainApp.propTypes = {
    dashboardComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
      .isRequired,
    loginComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
      .isRequired,
    notFoundComponent: PropTypes.oneOfType([PropTypes.object, PropTypes.func])
      .isRequired,
    user: PropTypes.shape.isRequired,
  };
export default MainApp;
