import { Redirect, Route } from 'react-router-dom';

import PropTypes from 'prop-types';
import React from 'react';

function PrivateRouter({ component: Component, isAuthenticated, to, ...rest }) {
  return (
    <Route
      {...rest}
      render={items =>
        isAuthenticated ? (
          <Component {...items} />
        ) : (
          <Redirect
            to={{
              pathname: to,
              state: { redirect: items.location.pathname, isAuthenticated },
            }}
          />
        )
      }
    />
  );
}

PrivateRouter.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  to: PropTypes.string,
};

PrivateRouter.defaultProps = {
  to: '/',
};

export default PrivateRouter;
