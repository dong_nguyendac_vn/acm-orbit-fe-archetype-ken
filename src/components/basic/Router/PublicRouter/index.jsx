import { Redirect, Route } from 'react-router-dom';

import { AppConfig } from 'config';
import PropTypes from 'prop-types';
import React from 'react';

function RoutePublic({ component: Component, isAuthenticated, to, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        isAuthenticated ? <Redirect to={to} /> : <Component {...props} />
      }
    />
  );
}

RoutePublic.propTypes = {
  component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  to: PropTypes.string,
};

RoutePublic.defaultProps = {
  to: `${AppConfig.appPrefix}`,
};

export default RoutePublic;
