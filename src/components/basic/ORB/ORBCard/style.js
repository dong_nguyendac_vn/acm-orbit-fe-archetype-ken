import { borderRadius, boxShadow } from 'styles';

import { Card } from 'antd';
import { palette } from 'styled-theme';
import styled from 'styled-components';

const CardStyled = styled(Card)`
  &.ant-card {
    ${borderRadius('6px')};
    ${boxShadow('1px 6px 9px 0 rgba(144, 164, 183, 0.22)')};
  }
  & .ant-card-head {
    border-bottom: none;
    & .ant-card-head-title {
      font-size: 20px;
      color: ${palette('primary', 0)};
    }
  }
`;

export default CardStyled;
