import { borderRadius, transition } from 'styles';

import { Input } from 'antd';
import { palette } from 'styled-theme';
import styled from 'styled-components';

const InputStyled = styled(Input)`
  &.ant-input {
    padding: 4px 10px;
    width: 100%;
    height: 35px;
    cursor: text;
    text-align: left;
    font-size: 13px;
    line-height: 1.5;
    color: ${palette('text', 1)};
    background-color: #fff;
    background-image: none;
    border: 1px solid ${palette('border', 0)};
    ${borderRadius('3px')};
    ${transition()};

    &:focus {
      border-color: ${palette('primary', 0)};
    }

    &.ant-input-lg {
      height: 42px;
      padding: 6px 10px;
    }

    &.ant-input-sm {
      padding: 1px 10px;
      height: 30px;
    }

    &::-webkit-input-placeholder {
      text-align: left;
      color: ${palette('grayscale', 0)};
    }

    &:-moz-placeholder {
      text-align: left;
      color: ${palette('grayscale', 0)};
    }

    &::-moz-placeholder {
      text-align: left;
      color: ${palette('grayscale', 0)};
    }
    &:-ms-input-placeholder {
      text-align: left;
      color: ${palette('grayscale', 0)};
    }
  }
`;
export default InputStyled;
