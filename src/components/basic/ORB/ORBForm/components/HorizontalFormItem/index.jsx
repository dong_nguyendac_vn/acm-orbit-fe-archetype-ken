import PropTypes from 'prop-types';
import React from 'react';
import Form from '../Form';

const itemHorizontalLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 24 },
    lg: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 24 },
    md: { span: 24 },
    lg: { span: 16 },
  },
};

const { Item } = Form;

export function HorizontalFormItem(props) {
  const { children, ...rest } = props;
  return (
    <Item className="op-form-horizontal" {...itemHorizontalLayout} {...rest}>
      {children}
    </Item>
  );
}

HorizontalFormItem.propTypes = {
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired,
};

export default HorizontalFormItem;
