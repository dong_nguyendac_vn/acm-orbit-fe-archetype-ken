import { font, palette } from 'styled-theme';

import { Form } from "antd";
import styled from 'styled-components';

const FormStyled = styled(Form)`
  font-family: ${font("primary")} !important;
  .ant-form-explain {
    margin-top: 5px;
  }
  .op-form-horizontal {
    .ant-form-item-label {
      text-align: left;
      & > label {
        color: ${palette("text", 2)};
      }
    }
  }
`;

export default FormStyled;
