import { ORBButton, ORBForm, ORBInput } from 'components/basic/ORB';

import styled from 'styled-components';

const LoginFormStyled = styled(ORBForm)`
  min-width: 320px;
  padding: 0 15px;
  @media (min-width: 768px) {
    min-width: 380px;
  }
`;

const LoginButtonStyled = styled(ORBButton)`
  width: 100%;
  margin-top: 15px;
  font-size: 18px !important;
  font-weight: 700 !important;
  height: 50px !important;
`;

const LoginInputStyled = styled(ORBInput)`
  height: 49px !important;
  font-size: 16px !important;
  line-height: 0.95 !important;
`;

export { LoginFormStyled, LoginButtonStyled, LoginInputStyled };
