
import { ORBForm } from 'components/basic/ORB/ORBForm';
import PropTypes from 'prop-types';
import React from 'react';
import { LoginButtonStyled, LoginFormStyled, LoginInputStyled } from './style';

const { Item } = ORBForm;

class LoginForm extends React.PureComponent {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    const { form } = this.props;
    form.validateFields((err, values) => {
      if (!err) {
        console.log(values);
      }
    });
  }

  render() {
    const { form } = this.props;
    const { getFieldDecorator } = form;
    return (
      <LoginFormStyled
        className="login-form"
        autoComplete="off"
        onSubmit={this.handleSubmit}
      >
        <Item>
          {getFieldDecorator('username', {
            rules: [{ required: true, message: 'Please input your user name' }],
          })(
            <LoginInputStyled
              data-testid="UserInput"
              size="large"
              placeholder="User Name"
            />,
          )}
        </Item>
        <Item>
          {getFieldDecorator('password', {
            rules: [{ required: true, message: 'Please input your password!' }],
          })(
            <LoginInputStyled
              data-testid="PasswordInput"
              size="large"
              type="password"
              placeholder="Password"
            />,
          )}
        </Item>
        <Item>
          <LoginButtonStyled
            data-testid="LoginButton"
            type="primary"
            size="large"
            className="btn-login"
            htmlType="submit"
          >
            LOG IN
          </LoginButtonStyled>
        </Item>
      </LoginFormStyled>
    );
  }
}
LoginForm.propTypes = {
    form: PropTypes.shape.isRequired,
};

export default LoginForm;
