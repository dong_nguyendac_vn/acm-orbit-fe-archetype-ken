import React, { PureComponent } from 'react';

import PropTypes from 'prop-types';
import LoginForm from './components/Form/Login';

class LoginScreen extends PureComponent {
  
  render() {
    const { form } = this.props;
    return (
      <>
        <LoginForm form={form} />
      </>
    );
  }
}
LoginScreen.propTypes = {
    form: PropTypes.shape.isRequired,
};
export default LoginScreen;
