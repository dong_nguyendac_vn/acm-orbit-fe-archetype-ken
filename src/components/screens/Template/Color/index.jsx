import React, { PureComponent } from 'react';

import { ORBCard } from 'components/basic/ORB';

class ColorTemplateScreen extends PureComponent {
  render() {
    return <ORBCard>Color</ORBCard>;
  }
}

export default ColorTemplateScreen;
