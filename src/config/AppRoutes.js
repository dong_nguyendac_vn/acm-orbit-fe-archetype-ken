import { lazy } from 'react';

 const AppRoutes = [
  {
    path: '/',
    exact: true,
    component: lazy(() => import('containers/screens/Home')),
  },
  {
    path: '/404',
    component: lazy(() => import('containers/screens/NotFound')),
  },
];

export default AppRoutes;
