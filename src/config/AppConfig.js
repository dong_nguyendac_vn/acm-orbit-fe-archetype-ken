const AppConfig = {
  appPrefix: '/dashboard',
  dateFormat: 'DD/MM/YYYY',
  dateTimeFormat: 'DD/MM/YYYY HH:mm:ss',
};
export default AppConfig;