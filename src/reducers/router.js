import { connectRouter } from 'connected-react-router';
import history from 'utils/history';

export default {
  router: connectRouter(history),
};
