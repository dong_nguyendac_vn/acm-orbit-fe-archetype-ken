import router from './router';
import user from './user';

export default { ...user, ...router };
