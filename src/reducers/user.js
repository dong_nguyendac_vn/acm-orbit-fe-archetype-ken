import ActionTypes from 'actions/user/type';
import { handleActions } from 'redux-actions';
import immutable from 'immutability-helper';

const defaultState = {
  isAuthenticated: false,
  auth: {
    id: -1,
    name: '',
    permissions: [],
  },
};

export default {
  user: handleActions(
    {
      [ActionTypes.USER_LOGIN]: state => immutable(state, {}),
      [ActionTypes.USER_LOGIN_SUCCESS]: (state, { payload }) =>
        immutable(state, {
          isAuthenticated: { $set: true },
          auth: {
            id: { $set: payload.data.id },
            name: { $set: payload.data.name },
            permissions: { $set: payload.data.permissions },
          },
        }),
      [ActionTypes.USER_LOGOUT]: state => immutable(state, {}),
      [ActionTypes.USER_LOGOUT_SUCCESS]: state =>
        immutable(state, {
          isAuthenticated: { $set: false },
          auth: { permissions: { $set: [] } },
        }),
    },
    defaultState,
  ),
};
