import { all, takeLatest } from 'redux-saga/effects';

import ActionTypes from 'actions/user/type';

export function* authenticate() {
  // implement later
}

/**
 * Logout
 */
export function* logout() {
  // implement later
}

export default function* root() {
  yield all([
    takeLatest(ActionTypes.USER_LOGIN, authenticate),
    takeLatest(ActionTypes.USER_LOGOUT, logout),
  ]);
}
