export default function borderRadius(radius = 0) {
  return `
        -webkit-border-radius: ${radius};
        -moz-border-radius: ${radius};
        -ms-transition: ${radius};
        -o-border-radius: ${radius};
        border-radius: ${radius};
    `;
}
