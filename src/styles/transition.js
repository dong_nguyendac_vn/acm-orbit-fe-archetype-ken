export default function transition(timing = 0.3) {
  return `
        -webkit-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
        -moz-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
        -ms-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
        -o-transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
        transition: all ${timing}s cubic-bezier(0.215, 0.61, 0.355, 1);
    `;
}
